package ro.ubb.catalog.web.dto.DtoList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.ubb.catalog.web.dto.DtoEntity.PurchaseDto;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PurchasesDto {
    private Set<PurchaseDto> purchases;
}
