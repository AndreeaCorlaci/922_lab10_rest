package ro.ubb.catalog.web.dto.SortedDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.ubb.catalog.web.dto.DtoEntity.PurchaseDto;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SortedPurchasesDto {
    private List<PurchaseDto> purchases;
}
