package ro.ubb.catalog.web.dto.SortedDto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.ubb.catalog.web.dto.DtoEntity.ClientDto;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SortedClientsDto {
    private List<ClientDto> clients;
}
