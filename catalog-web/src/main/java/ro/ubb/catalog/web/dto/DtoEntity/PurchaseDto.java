package ro.ubb.catalog.web.dto.DtoEntity;

import lombok.*;
import ro.ubb.catalog.web.dto.BaseDto;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class PurchaseDto extends BaseDto {
    private Long idClient;
    private Long idBook;
    private Date date;
}
