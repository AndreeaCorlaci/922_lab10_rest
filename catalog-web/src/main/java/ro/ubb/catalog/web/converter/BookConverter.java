package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.web.dto.DtoEntity.BookDto;
@Component
public class BookConverter extends BaseConverter<Book,BookDto> {
    @Override
    public Book convertDtoToModel(BookDto dto) {
        Book book = Book.builder()
                .author(dto.getAuthor())
                .title(dto.getTitle())
                .price(dto.getPrice())
                .build();
        book.setId(dto.getId());
        return book;
    }

    @Override
    public BookDto convertModelToDto(Book book) {
        BookDto dto = BookDto.builder()
                .author(book.getAuthor())
                .title(book.getTitle())
                .price(book.getPrice())
                .build();
        dto.setId(book.getId());
        return dto;
    }
}
