package ro.ubb.catalog.web.dto.DtoEntity;

import lombok.*;
import ro.ubb.catalog.web.dto.BaseDto;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class BookDto extends BaseDto {
    private String title;
    private String author;
    private int price;

}
