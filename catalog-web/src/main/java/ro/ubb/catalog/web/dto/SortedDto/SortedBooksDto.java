package ro.ubb.catalog.web.dto.SortedDto;

import ro.ubb.catalog.web.dto.DtoEntity.BookDto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class SortedBooksDto {

    private List<BookDto> books;
}
