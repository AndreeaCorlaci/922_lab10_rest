package ro.ubb.catalog.core.service.ServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Validators.BookValidator;
import ro.ubb.catalog.core.repository.DBRepository.BookRepository;
import ro.ubb.catalog.core.service.ServiceInterfaces.BookService;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    public static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;

    @Override
    public Book saveBook(Book book) throws ValidatorException {
        BookValidator.validate(book);
        log.trace("saveBook --- method entered");
        return bookRepository.save(book);

    }

    @Override
    @Transactional
    public Book updateBook(Long id, Book b) throws ValidatorException {
        log.trace("updateBook - method entered:id={} book={}",id, b);
        BookValidator.validate(b);
        Optional<Book> optionalBook =  bookRepository.findById(id);
        optionalBook.ifPresent(s -> {
            s.setTitle(b.getTitle());
            s.setAuthor(b.getAuthor());
            s.setPrice(b.getPrice());
            log.debug("updateBook - updated: s={}", s);
        });
        log.trace("updateBook - method finished");
        return optionalBook.orElse(null);

    }

    @Override
    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public List<Book> getAllBooks() {
        log.trace("getAllBooks --- method entered");
        List<Book> result = bookRepository.findAll();
        log.trace("getAllBooks: result={}", result);
        return result;
    }

    @Override
    public Book getBook(Long id) {
        return bookRepository.findById(id).orElse(null);
    }

    @Override
    public List<Book> getSortedBooks() {
        Sort sort = Sort.by("price").ascending().and( Sort.by("title").descending());
        return bookRepository.findAll(sort);
    }
}
