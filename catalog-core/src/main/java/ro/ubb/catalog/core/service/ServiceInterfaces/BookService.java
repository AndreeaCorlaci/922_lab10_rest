package ro.ubb.catalog.core.service.ServiceInterfaces;


import org.springframework.data.domain.Sort;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;

import java.util.List;

public interface BookService {
    Book saveBook(Book book) throws ValidatorException;
    Book updateBook(Long id,Book b) throws ValidatorException;
    void deleteBook(Long id);
    List<Book> getAllBooks();
    Book getBook(Long id);
    List<Book> getSortedBooks();
}
