package ro.ubb.catalog.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.service.ServiceInterfaces.BookService;
import ro.ubb.catalog.core.service.ServiceInterfaces.ClientService;
import ro.ubb.catalog.core.service.ServiceInterfaces.PurchaseService;


import java.util.*;
import java.util.stream.Collectors;

@Service
public class Reports {
    @Autowired
    private BookService bookService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private PurchaseService purchaseService;


    public Set<Client> filterClientsWithMoreThanXBooks(int no) {
        return clientService.getAllClients().stream().
                filter(x ->
                        no < purchaseService.getAllPurchases().stream().
                                filter(y -> y.getIdClient().equals(x.getId())).count()).
                collect(Collectors.toSet());
    }

    public Integer spentMoney(Long clientId) {
        Set<Long> bookIds = purchaseService.getAllPurchases().stream().filter(x -> x.getIdClient().
                equals(clientId)).map(Purchase::getIdBook).collect(Collectors.toSet());
        return bookService.getAllBooks().stream().filter(x -> bookIds.contains(x.getId())).
                map(Book::getPrice).reduce(0, Integer::sum);
    }

    public List<Client> sortClientsByMoneySpent() {

        List<Client> clients = new ArrayList<>(clientService.getAllClients());
        clients.sort(Comparator.comparing(c -> spentMoney(c.getId())));
        return clients;
    }

    public List<Book> filterBooksByTitles(String s) {
        List<Book> filteredBooks = bookService.getAllBooks();
        filteredBooks.removeIf(Book -> !Book.getTitle().contains(s));
        return filteredBooks;
    }
    public List<Book> removeBookIfPriceLessThenS(int s) {
        List<Book> filteredBooks = bookService.getAllBooks();
        filteredBooks.removeIf(book -> book.getPrice() <= s);
        return filteredBooks;
    }

    public List<Client> filterClientsByName(String s) {
        List<Client> filteredClients = clientService.getAllClients();
        filteredClients.removeIf(client -> !client.getName().contains(s));
        return filteredClients;
    }

    public  Set<Purchase> filterPurchasesByDateBefore(Date date) {
        return purchaseService.getAllPurchases().stream().filter(x -> x.getDate().before(date)).
                collect(Collectors.toSet());
    }


    public  Set<Purchase> getAllPurchaseFromClientX(Long idClient) {
        return purchaseService.getAllPurchases().stream().filter(x -> x.getIdClient().equals(idClient)).
                collect(Collectors.toSet());
    }

    public  Set<Purchase> getAllPurchaseofBookX(Long idBook) {
        return purchaseService.getAllPurchases().stream().filter(x -> x.getIdBook().equals(idBook)).
                collect(Collectors.toSet());
    }

}
