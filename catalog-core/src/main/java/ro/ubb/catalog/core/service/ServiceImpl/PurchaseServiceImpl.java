package ro.ubb.catalog.core.service.ServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.model.Validators.PurchaseValidator;
import ro.ubb.catalog.core.repository.DBRepository.PurchaseRepository;
import ro.ubb.catalog.core.service.ServiceInterfaces.PurchaseService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PurchaseServiceImpl implements PurchaseService {
    public static final Logger log = LoggerFactory.getLogger(PurchaseServiceImpl.class);

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Override
    public Purchase savePurchase(Purchase p) throws ValidatorException {
        PurchaseValidator.validate(p);
        return purchaseRepository.save(p);
    }

    @Override
    @Transactional
    public Purchase updatePurchase(Purchase p) throws ValidatorException {
        log.trace("updatePurchase - method entered: purchase={}",p);
        //PurchaseValidator.validate(p);
        Optional<Purchase> optionalPurchase =  purchaseRepository.findById(p.getId());
        optionalPurchase.ifPresent(pp->{
                    pp.setDate(p.getDate());
                    pp.setIdBook(p.getIdBook());
                    pp.setIdClient(p.getIdClient());
                    log.debug("updatePurchase - updated: purchase={}",pp);
                });
        log.trace("updatePurchase- method finished");
        return optionalPurchase.orElse(null);
    }

    @Override
    public void deletePurchase(Long idClient, Long idBook) {
        Long id = findPurchase(idClient,idBook);
        purchaseRepository.deleteById(id);
    }

    @Override
    public List<Purchase> getAllPurchases() {
        return purchaseRepository.findAll();
    }

    @Override
    public Long findPurchase(Long idClient, Long idBook) {
        List<Purchase> listP = new ArrayList<>(purchaseRepository.findAll());
        Optional<Purchase> optional=  listP.stream().filter(x->x.getIdClient().equals(idClient) && x.getIdBook().equals(idBook)).findFirst();
        if (optional.isPresent()) return optional.get().getId();
        return -1L;
    }

    @Override
    public Purchase getPurchase(Long id) {
        return purchaseRepository.findById(id).orElse(null);
    }

    @Override
    public  List<Purchase> getSortedPurchases() {
        Sort sort = Sort.by("idClient").and(Sort.by("idBook"));
        return purchaseRepository.findAll(sort);
    }
}
