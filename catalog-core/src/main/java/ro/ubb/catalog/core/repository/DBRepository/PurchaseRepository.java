package ro.ubb.catalog.core.repository.DBRepository;


import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.repository.SortingRepository;

public interface PurchaseRepository extends SortingRepository<Purchase,Long> {
}
