package ro.ubb.catalog.core.repository.DBRepository;

import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.repository.SortingRepository;


public interface BookRepository extends SortingRepository<Book,Long> {
}
