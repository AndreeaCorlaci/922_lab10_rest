package ro.ubb.catalog.core.service.ServiceInterfaces;


import org.springframework.data.domain.Sort;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;

import java.util.List;

public interface ClientService {
    Client saveClient(Client c) throws ValidatorException;
    Client updateClient(Long id,Client c) throws ValidatorException;
    void deleteClient(Long id);
    List<Client> getAllClients();
    Client getClient(Long id);
    List<Client> getSortedClients();
}
