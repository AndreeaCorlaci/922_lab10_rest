package ro.ubb.catalog.core.repository.DBRepository;

import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.repository.SortingRepository;


public interface ClientRepository extends SortingRepository<Client,Long> {
}
