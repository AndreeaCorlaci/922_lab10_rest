package ro.ubb.catalog.core.service.ServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Validators.ClientValidator;
import ro.ubb.catalog.core.repository.DBRepository.ClientRepository;
import ro.ubb.catalog.core.service.ServiceInterfaces.ClientService;


import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {
    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client saveClient(Client c) throws ValidatorException {
        ClientValidator.validate(c);
        return clientRepository.save(c);
    }

    @Override
    @Transactional
    public Client updateClient(Long id,Client client) throws ValidatorException {
        log.trace("updateClient - method entered:id{}, client={}",id,client);
        //ClientValidator.validate(client);
        Optional<Client> optionalClient =  clientRepository.findById(id);
        optionalClient.ifPresent(c->{
                    c.setName(client.getName());

                    log.debug("updateClient - updated: client={}",c);
                });
        log.trace("updateClient- method finished");
        return optionalClient.orElse(null);
    }

    @Override
    public void deleteClient(Long id) {
        clientRepository.deleteById(id);
    }

    @Override
    public List<Client> getAllClients() {
        log.trace("getAllClients --- method entered");
        List<Client> result =  clientRepository.findAll();
        log.trace("getAllClients: result={}", result);
        return result;
    }

    @Override
    public Client getClient(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    @Override
    public List<Client> getSortedClients() {
        Sort sort  = Sort.by("name").descending();
        return clientRepository.findAll(sort);
    }
}
